import { gql } from "apollo-boost";

export const CREATE_CHAT = gql`
  mutation createChat($username: String, $message: String) {
    createChat(username: $username, message: $message) {
      id
    }
  }
`;



export const GET_CHATS = gql`{
  getChats {
   id
   username
   message
  }
}
`;


export const SUBSCRIBE_CHAT = gql`
  subscription chatSent {
    chatSent {
      id
      username
      message
    }
}
`;
