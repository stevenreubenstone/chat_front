import React, { Component } from 'react';
import { Mutation } from "react-apollo";
import { CREATE_CHAT } from ".././queries.js";


class AddComment extends Component {

    state = {
        username: '',
        message: '',
    };


    handleInput = (e) => {
        const formData = {};
        formData[e.target.name] = e.target.value;
        this.setState({ ...formData });
    };


    render() {
        return (
            <Mutation mutation={CREATE_CHAT}>
                {createChat => (
                    <div className="Center">
                        <div className="Card">
                            <form onSubmit={e => {
                                e.preventDefault();
                                this.setState({ message: '' })
                                createChat({ variables: { username: this.state.username, message: this.state.message } })
                            }}
                            >
                                <div className="Center">
                                    <div>
                                        <textarea
                                            name="username"
                                            onChange={this.handleInput}
                                            type="text"
                                            placeholder="Enter your username.."
                                            className="Box"
                                        />
                                        <br></br>
                                        <textarea
                                            name="message"
                                            onChange={this.handleInput}
                                            type="text"
                                            placeholder="Enter your chat message.."
                                            className="Box2"
                                            value={this.state.message}
                                        />
                                        <br></br>
                                        <div className="Center">
                                            <button>Send Chat</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                )}
            </Mutation>
        );
    }
}

export default AddComment;