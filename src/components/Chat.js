import React, { Component } from 'react';


class Chat extends Component {


    scrollToBottom = () => {
        this.messagesEnd.scrollIntoView({ behavior: "smooth" });
    }

    componentDidMount() {
        this.props.subscribeToNewComments();
        this.scrollToBottom();

    }

    componentDidUpdate() {
        this.scrollToBottom();
    }

    render() {
        console.log('data down here:', this.props.data)
        return (
            <div className="Container" >
                {this.props.data.getChats.map(item => {
                    return <div className="Center">
                        <div style={{ padding: '7px', maxWidth: '800px', minWidth: '400px', borderRadius: '11px', boxShadow: '2px 2px #888888', backgroundColor: '#DBDBFF', marginBottom: '10px' }}>{item.username}: {item.message} </div>
                    </div>
                })}
                <div ref={(el) => { this.messagesEnd = el; }} />
            </div>
        );
    }
}

export default Chat;