import React, { Component } from 'react';
import { Query } from "react-apollo";
import { GET_CHATS } from ".././queries.js";
import { SUBSCRIBE_CHAT } from ".././queries.js";
import Chat from "./Chat";



class ChatControl extends Component {
    render() {
        return (
            <Query query={GET_CHATS} fetchPolicy="network-only">
                {({ loading, error, data, subscribeToMore }) => {
                    if (loading) return 'Loading...';
                    if (error) return 'There is error';
                    // console.log('data:', data)
                    return (
                        <Chat data={data}
                            subscribeToNewComments={() =>
                                subscribeToMore({
                                    document: SUBSCRIBE_CHAT,
                                    updateQuery: (prev, { subscriptionData }) => {
                                        if (!subscriptionData.data) return prev;
                                        const chatItem = subscriptionData.data.chatSent;
                                        console.log('subscription working:', chatItem)
                                        return Object.assign({}, prev, {
                                            getChats: [...prev.getChats, chatItem]
                                        });
                                    }
                                })
                            } />
                    )
                }}
            </Query>
        );
    }
}

export default ChatControl;