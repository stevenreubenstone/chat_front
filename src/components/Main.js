import React, { Component } from 'react';
import AddComment from './AddComment';
import ChatControl from './ChatControl';
import logo from './think1.png';



class Main extends Component {
    render() {
        return (
            <div>
                <div className="Center">
                    <img src={logo} height="250.25" width="357.5" style={{ marginTop: 15 }} />

                </div>
                <ChatControl />
                <AddComment />
            </div>
        );
    }
}

export default Main;