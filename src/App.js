import React, { Component } from 'react';
import './App.css';
import Main from './components/Main';
import { ApolloProvider } from "react-apollo";
import { setContext } from 'apollo-link-context';
import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { HttpLink } from 'apollo-link-http';
import { WebSocketLink } from 'apollo-link-ws';
import { split } from 'apollo-link';
import { getMainDefinition } from 'apollo-utilities';


// PRODUCTION
const httpLink = new HttpLink({
  uri: "https://mskchatback.herokuapp.com/graphql",
  //credentials: 'include'
});

// LOCAL
// const httpLink = new HttpLink({
//   uri: "https://6f1633a7.ngrok.io/graphql",
//   //credentials: 'include'
// });


const authLink = setContext(async (_, { headers }) => {
  // const toker = await Expo.SecureStore.getItemAsync("fbToken");
  //console.log('toker here', toker)
  return {
    headers: {
      ...headers,
      // fbtoken: toker ? `${toker}` : ""
    }
  }
});

// PRODUCTION

const wsLink = new WebSocketLink({
  uri: `wss://mskchatsubscriptions.herokuapp.com/subscriptions`,
  options: {
    reconnect: true
  }
});

// LOCAL
// const wsLink = new WebSocketLink({
//   uri: `ws://0112408b.ngrok.io/subscriptions`,
//   options: {
//     reconnect: true
//   }
// });







const link = split(
  // split based on operation type
  ({ query }) => {
    const { kind, operation } = getMainDefinition(query);
    return kind === 'OperationDefinition' && operation === 'subscription';
  },
  wsLink,
  httpLink,
);


const client = new ApolloClient({
  link,
  cache: new InMemoryCache()
});






class App extends Component {
  render() {
    return (
      <ApolloProvider client={client}>
        <Main />
      </ApolloProvider>
    );
  }
}

export default App;