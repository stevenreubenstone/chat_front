
## Idea Chat: How it Works

The Idea Chat allows researchers and clinicians inside an organization to chat inside of a single "Idea Chat" channel. IF a chat user mentions the word "idea" in their chat meessage, the site administrator will be sms'd this message to their cell phone via Twilio!

## Getting Set up Locally

Here are a few steps to get up and running locally. However you can access the functioning app live on https://mskchatfront.herokuapp.com/!


### chat_back repo

- Clone repo
- Install dependencies (npm i)
- Create Postgres database on local machine (get proper connection creds)
- Run Redis on local (get proper connection creds)
- Create .env file
```
PGCONNECTSTRING='postgres://localhost/mskchat'
REDDISHOST='localhost'
REDDISPORT = 6379
PORT=4000
TWILIO1=Must use your own twilio account in dev.
TWILIO2=Must use your own twilio account in dev.
```
- run knex migrate:latest to perform migrations on pg database
- ```npm run server```
- Set up local tunnel URL for port 4000 (with local tunnel tool, e.g., ngrok)

**Unit Test**

Run ```npm run test``` to see the validity of the test cases in util.test.js (function (unit) being tested coming from testFunction.js).


### chatSockets repo

- Clone repo
- Install dependencies (npm i)
- Create .env file
```
PORT=5000
REDDISHOST='localhost'
REDDISPORT = 6379
REDDISPW=
```

- ```npm run server```
- Set up local tunnel URL for port 5000 (with local tunnel tool, e.g., ngrok)


### chat_front repo

- Clone repo
- Open App.js: Update httpLink url with local tunnel url for port 4000, and update wsLink for local tunnel url 5000
- ```npm start```
- Use the Idea Chat! 







